import express, { NextFunction, Request, Response } from 'express'
import routes from './routes'
import helmet from 'helmet'

const app = express()

app.use(helmet())
app.use(express.json())
// ===================== Basic Route =====================
// app.get('/', (req: Request, res: Response) => {
// 	return res.redirect('http://example.com')
// })

// app.post('/api/data', (req: Request, res: Response) => {
// 	console.log(req.body)
// 	return res.sendStatus(200)
// })

// app.all('/api/all', (req: Request, res: Response) => {
// 	return res.sendStatus(200)
// })

// ===================== Route Group =====================
// app
// 	.route('/api/books')
// 	.get((req: Request, res: Response) => {
// 		return res.send('You make a GET request')
// 	})
// 	.post((req: Request, res: Response) => {
// 		return res.send('You make a POST request')
// 	})
// 	.put((req: Request, res: Response) => {
// 		return res.send('You make a PUT request')
// 	})
// 	.delete((req: Request, res: Response) => {
// 		return res.send('You make an DELETE request')
// 	})

// ===================== Additional Route =====================
// app.get('/health', (req: Request, res: Response) => res.sendStatus(200))
// app.get('/ab*cd', (req: Request, res: Response) => res.send('/ab*cd'))
// app.get(/abc/, (req: Request, res: Response) => res.send('abc'))

// ===================== Middlewares =====================
// const middleware =
// 	({ name }: { name: string }) =>
// 	(req: Request, res: Response, next: NextFunction) => {
// 		res.locals.name = name
// 		next()
// 	}

// app.use(middleware({ name: 'TomDoesTech' }))

// routes(app)
// ===================== Error Handling =====================
// async function throwsError() {
// 	throw new Error('Boom!')
// }

// app.get('/error', async (req, res) => {
// 	try {
// 		await throwsError()
// 	} catch (e) {
// 		res.status(400).send('Something bad happened')
// 	}
// })

app.listen(3000, () => {
	console.log('Application listening at http://localhost:3000')
})
