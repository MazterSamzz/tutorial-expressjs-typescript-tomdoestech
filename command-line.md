<!-- ts config -->
npx tsc --init
<!-- Init Yarn -->
yarn init
<!-- Install ts-node & typescript -->
yarn add ts-node-dev typescript -D

<!-- Install Express -->
yarn add express
yarn add @types/node @types/express -D